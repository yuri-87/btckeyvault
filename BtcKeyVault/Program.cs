﻿using Microsoft.Azure.KeyVault;
using Microsoft.Azure.KeyVault.WebKey;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using NBitcoin;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace BtcKeyVault
{
    class Program
    {
        private static string APP_ID = "edb12633-c979-4b61-84a2-4ef23e9ac3a3";
        private static string APP_PASSWORD = "Bs3kqITi5dH3vvi6c6nKTaDV4nFAjzDKERnbtaHMPUk=";
        private static string URI = "https://btc-keys-test.vault.azure.net/";

        static void Main(string[] args)
        {
            try
            {
                var kvc = new KeyVaultClient(new KeyVaultClient.AuthenticationCallback(GetToken));
                var network = Network.TestNet;

                var key = new Key(false);
                var kid = AddKeyToKeyVault(kvc, key);
                var keyAddress = key.ScriptPubKey.Hash.GetAddress(network).ToString();

                // здесь нужно остановиться, отправить монеты на этот адрес и вписать параметры входящей транзакции
                var utxoTxId = uint256.Parse("db3d662ba005fbc364eae3e0c2eb3de37614a9b408ac3d5f575aabf5ef5ee8ec");
                var utxoVout = (uint)0;
                var utxoAmount = new Money(0.00015m, MoneyUnit.BTC);
                var utxoAddress = BitcoinAddress.Create(keyAddress, network);

                var tx = network.CreateTransaction();
                tx.Inputs.Add(new OutPoint(utxoTxId, utxoVout));
                tx.Outputs.Add(new Money(0.0001m, MoneyUnit.BTC), BitcoinAddress.Create(keyAddress, network));

                var signatureHash = tx.GetSignatureHash(new Coin(utxoTxId, utxoVout, utxoAmount, new Script(key.ScriptPubKey.ToString())));

                var signature = kvc.SignAsync(kid.Kid, "ES256K", signatureHash.ToBytes()).Result.Result;



                //var verifyResult = kvc.VerifyAsync(URI, kid.Name, kid.Version, "ES256K", signatureHash.ToBytes(), signature).Result;

                var encodedSignature = encode(signature, SigHash.All);

                tx.Inputs[0].ScriptSig = new Script(
                    Op.GetPushOp(encodedSignature),
                    Op.GetPushOp(new PubKey(key.PubKey.ToHex()).ToBytes()),
                    Op.GetPushOp(key.ScriptPubKey.ToBytes()));

                var transactionHex = tx.ToHex();
            }
            catch (Exception ex)
            {
            }
        }

        // подсмотрел здесь https://github.com/bitcoinjs/bitcoinjs-lib/blob/master/src/script_signature.js
        private static byte[] toDER(byte[] x)
        {
            var zero = new byte[1] { 0 };
            var i = 0;
            while (x[i] == 0) ++i;
            if (i == x.Length) return zero;
            x = x.Skip(i).ToArray();
            if ((x[0] & 0x80) > 0) return zero.Concat(x).ToArray();
            return x;
        }

        // подсмотрел здесь https://github.com/bitcoinjs/bip66/blob/master/index.js
        private static byte[] bip66Encode(byte[] r, byte[] s)
        {
            var lenR = r.Length;
            var lenS = s.Length;
            if (lenR == 0) throw new InvalidOperationException("R length is zero");
            if (lenS == 0) throw new InvalidOperationException("S length is zero");
            if (lenR > 33) throw new InvalidOperationException("R length is too long");
            if (lenS > 33) throw new InvalidOperationException("S length is too long");
            if ((r[0] & 0x80) > 0) throw new InvalidOperationException("R value is negative");
            if ((s[0] & 0x80) > 0) throw new InvalidOperationException("S value is negative");
            if (lenR > 1 && (r[0] == 0x00) && (r[1] & 0x80) == 0) throw new InvalidOperationException("R value excessively padded");
            if (lenS > 1 && (s[0] == 0x00) && (s[1] & 0x80) == 0) throw new InvalidOperationException("S value excessively padded");

            var signature = new byte[6 + lenR + lenS];

            // 0x30 [total-length] 0x02 [R-length] [R] 0x02 [S-length] [S]
            signature[0] = 0x30;
            signature[1] = (byte)(signature.Length - 2);
            signature[2] = 0x02;
            signature[3] = (byte)r.Length;
            r.CopyTo(signature, 4);
            signature[4 + lenR] = 0x02;
            signature[5 + lenR] = (byte)s.Length;
            s.CopyTo(signature, 6 + lenR);

            return signature;
        }

        // подсмотрел здесь https://github.com/bitcoinjs/bitcoinjs-lib/blob/master/src/script_signature.js
        private static byte[] encode(byte[] signature, SigHash hashType)
        {
            var hashTypeMod = (int)hashType & ~0x80;
            if (hashTypeMod <= 0 || hashTypeMod >= 4)
                throw new InvalidOperationException("Invalid hashType " + hashType);

            var r = toDER(signature.Take(32).ToArray());
            var s = toDER(signature.Skip(32).Take(32).ToArray());

            return bip66Encode(r, s).Concat(new byte[1] { (byte)((int)hashType) }).ToArray();
        }

        private static JsonWebKey AddKeyToKeyVault(KeyVaultClient kvc, Key key)
        {
            var ecParameters = new ECParameters
            {
                Curve = "P-256K",
                D = key.ToBytes(),
                X = key.PubKey.ToBytes().Skip(1).Take(32).ToArray(),
                Y = key.PubKey.ToBytes().Skip(33).Take(32).ToArray()
            };

            var keyId = key.PubKey.GetAddress(Network.TestNet).ToString();
            var keyBundle = kvc.ImportKeyAsync(URI, keyId, new JsonWebKey(ecParameters)
            {
                KeyOps = new[] { "sign", "verify" }
            }).Result;
            return keyBundle.Key;
        }

        public static async Task<string> GetToken(string authority, string resource, string scope)
        {
            var authContext = new AuthenticationContext(authority);
            var clientCred = new ClientCredential(APP_ID, APP_PASSWORD);
            var result = await authContext.AcquireTokenAsync(resource, clientCred);

            if (result == null)
                throw new InvalidOperationException("Failed to obtain the JWT token");

            return result.AccessToken;
        }
    }
}
